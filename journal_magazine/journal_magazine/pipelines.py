# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from pymongo import IndexModel, ASCENDING
import psycopg2
from psycopg2 import OperationalError, IntegrityError
import logging
from journal_magazine.items import JournalMagazineItem



class JournalMagazinePipeline(object):
    collection_name = "journal_magazine_items"

    def __init__(self, mongo_uri, mongo_db, postgres_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.postgres_db = postgres_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items'),
            postgres_db=crawler.settings.get("POSTGRES_DATABASE")
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        cursor = None
        try:
            connection = psycopg2.connect(self.postgres_db)
            connection.autocommit = True
            cursor = connection.cursor()
        except OperationalError as e:
            logging.info(e)

        data = self.db[self.collection_name].find()
        for row in data:
            try:
                insert_table_command = \
                """
                INSERT INTO ieee.journal_magazine
                    (publisher, title, cover, publication_link, publication_years, id)
                    VALUES(%s, %s, %s, %s, %s, %s);
                """
                params = \
                    (row['publisher'], row['title'], row['cover'], row['publication_link'], row['publication_years'], row['id'])

                cursor.execute(insert_table_command, params)
            except IntegrityError as e:
                try:
                    update_table_command = \
                        """
                        UPDATE ieee.journal_magazine
                          SET publisher=%s, title=%s, cover=%s, publication_link=%s, publication_years=%s 
                          WHERE id=%s;
                        """
                    params = \
                        (
                        row['publisher'], row['title'], row['cover'], row['publication_link'], row['publication_years'],
                        row['id'])

                    cursor.execute(update_table_command, params)
                except Exception as e:
                    logging.info(e)



        self.client.close()

    def process_item(self, item, spider):
        if isinstance(item, JournalMagazineItem):
            self.db[self.collection_name].insert_one(dict(item))
        return item
