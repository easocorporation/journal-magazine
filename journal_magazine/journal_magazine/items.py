# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class JournalMagazineItem(scrapy.Item):
    id = Field()
    publisher = Field()
    title = Field()
    cover = Field()
    publication_link = Field()
    publication_years = Field()

