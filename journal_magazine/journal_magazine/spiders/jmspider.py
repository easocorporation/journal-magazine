# -*- coding: utf-8 -*-

import scrapy, requests, json, logging, re
from scrapy.utils.project import get_project_settings
from journal_magazine.items import JournalMagazineItem


class JmspiderSpider(scrapy.Spider):
    name = 'jmspider'
    allowed_domains = ['ieeexplore.ieee.org']
    start_urls = ['http://ieeexplore.ieee.org/']

    def start_requests(self):
        end_point_url = "http://ieeexplore.ieee.org/rest/publication"
        cookies = requests.get("http://ieeexplore.ieee.org/").cookies.get_dict()
        payload = "{\"tabId\":\"title\",\"publisher\":\"\",\"collection\":\"\",\"contentType\":\"periodicals\"}"
        headers = get_project_settings().get('DEFAULT_REQUEST_HEADERS')
        response_obj = json.loads(requests.request("POST", end_point_url, data=payload, headers=headers, cookies=cookies).content.decode())
        records_per_page = response_obj['endRecord'] - response_obj['startRecord']
        total_records = response_obj['totalRecords']
        step = range(1, int(total_records / records_per_page) + 2)
        for i in step:
            cookies = requests.get("http://ieeexplore.ieee.org").cookies.get_dict()
            body = {"tabId": "title", "publisher": "", "collection": "", "pageNumber": str(i),"contentType": "periodicals"}
            yield scrapy.Request(url=end_point_url,
                                 callback=self.parse,
                                 method="POST",
                                 body=json.dumps(body),
                                 cookies=cookies)

    def parse(self, response):
        json_response = json.loads(response.body_as_unicode())
        item = JournalMagazineItem()
        objs = json_response['records']

        for obj in objs:
            try:
                item['id'] = obj['id']
            except KeyError as e:
                logging.info(e)

            try:
                item['publisher'] = obj['publisher']
            except KeyError as e:
                logging.info(e)

            try:
                item['title'] = obj['title']
            except KeyError as e:
                logging.info(e)

            try:
                item['cover'] = self.start_urls[0] + str(re.split(pattern=r".+ src='", string=obj['coverImageHtml'])[1].split("'")[0])
            except KeyError as e:
                logging.info(e)
            except AttributeError as e:
                logging.info(e)

            try:
                item['publication_link'] = self.start_urls[0] + obj['publicationLink']
            except KeyError as e:
                logging.info(e)

            try:
                item['publication_years'] = obj['allYears']
            except KeyError as e:
                logging.info(e)

            yield item